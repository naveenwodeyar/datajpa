package com.jpa.test;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Test
{
    public static void main(String[] args)
    {
        Set<Employee> empSet = new HashSet<>();
                        empSet.add(new Employee(25,"A","Development"));
                        empSet.add(new Employee(26,"B","Prod"));
                        empSet.add(new Employee(35,"C","QA"));
                        empSet.add(new Employee(36,"C","QA"));
                        empSet.add(new Employee(30,"D","HR"));

        empSet
                .stream().collect(Collectors.groupingBy(Employee::getEmpDept,Collectors.counting()))
                .entrySet()
                .forEach(System.out::println);

    }
}
