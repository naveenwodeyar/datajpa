package com.jpa.repo;

import com.jpa.modal.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepo extends JpaRepository<Student,Integer>
{

}
