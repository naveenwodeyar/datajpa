package com.jpa.controller;

import com.jpa.dto.RequestDTO;
import com.jpa.dto.StudentDTO;
import com.jpa.modal.Student;
import com.jpa.service.StudentService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController
{

    // Constructor Dependency Injection,
    @Autowired
    private StudentService studentService;

    // test endPoint
    @GetMapping("/wish")
    public ResponseEntity<String> testMethod()
    {
            String st = "Welcome to JPA!,";
            return new ResponseEntity<>(st,HttpStatus.TOO_EARLY);
    }

    // 1. PostRequest for sending the data,
    @PostMapping("/insertStudent")
    public ResponseEntity<RequestDTO> insertStudent(@RequestBody StudentDTO studentDTO)
    {
        Student st = studentService.insertStudents(studentDTO);
        RequestDTO requestDTO = new RequestDTO("Student added successfully,,",st);
        return new ResponseEntity<>(requestDTO, HttpStatus.CREATED);
    }

    // 2. GetRequest for receiving the data,
    @GetMapping("/getStudent/{stId}")
    public ResponseEntity<RequestDTO> getStudent(@PathVariable Integer stId)
    {
        Student st = studentService.getStudent(stId);
        RequestDTO requestDTO = new RequestDTO("Student Found,",st);
        return new ResponseEntity<>(requestDTO,HttpStatus.FOUND);
    }

    // 2.a get Students list,
    @GetMapping("/getStudents")
    public ResponseEntity<RequestDTO> getStudentsList()
    {
        List<Student> list = studentService.getStudents();
        RequestDTO requestDTO = new RequestDTO("Students found",list);
        return new ResponseEntity<>(requestDTO,HttpStatus.FOUND);
    }

    // 3. generateExcelFile,
    @GetMapping("/excel")
    public void generateExcel(HttpServletResponse response) throws IOException
    {
        response.setContentType("application/octet-stream");
        String headerKey= "Content-Disposition";
        String headerValue = "attachment;filename=Students.xls";

        response.setHeader(headerKey,headerValue);
        studentService.generateExcelFile(response);
    }
}
