package com.jpa.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO
{
    private String stName;

    private String stMail;

    private Long stContactNumber;

    private Double stClass;
}
