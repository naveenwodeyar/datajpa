package com.jpa.modal;

import com.jpa.dto.StudentDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "16-11-2023")
public class Student
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int stId;

    private String stName;

    private String stMail;

    private Long stContactNumber;

    private Double stClass;

    public Student(StudentDTO studentDTO)
    {
        this.stName = studentDTO.getStName();
        this.stMail = studentDTO.getStMail();
        this.stContactNumber = studentDTO.getStContactNumber();
        this.stClass = studentDTO.getStClass();
    }
}
