package com.jpa.service;

import com.jpa.dto.StudentDTO;
import com.jpa.modal.Student;
import com.jpa.repo.StudentRepo;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class StudentService
{
    // Constructor Dependency Injection,
    @Autowired
    private StudentRepo studentRepo;

    // CRUD operations,
    // 1. INSERT INTO TABLE-NAME VALUES();
        public Student insertStudents(StudentDTO studentDTO)
        {
            Student st = new Student(studentDTO);
                    return studentRepo.save(st);
        }

    // 2. SELECT * FROM TABLE-NAME WHERE id = "",
        public Student getStudent(Integer stId)
        {
            return studentRepo.findById(stId).get();
        }

    // 2.a get Students,
        public List<Student> getStudents()
        {
            return studentRepo.findAll();
        }

    // 3. UPDATE,
        public Student updateStudent(StudentDTO studentDTO,int stId)
        {
            Student st = studentRepo.findById(stId).get();
                    st.setStClass(studentDTO.getStClass());
                    st.setStMail(studentDTO.getStMail());
                    st.setStName(studentDTO.getStName());
                    st.setStContactNumber(studentDTO.getStContactNumber());

                    return studentRepo.save(st);
        }

    // 4. Generate Excel file,
        public void generateExcelFile(HttpServletResponse response) throws IOException
        {
            List<Student> students = studentRepo.findAll();

            // create Workbook,
            HSSFWorkbook workbook = new HSSFWorkbook();

            // create sheet,
            HSSFSheet sheet = workbook.createSheet("StudentsName");

            // create Row,
            HSSFRow row = sheet.createRow(0);

            //create cell's with name,
                    row.createCell(0).setCellValue("stId");
                    row.createCell(1).setCellValue("stName");
                    row.createCell(2).setCellValue("stMail");
                    row.createCell(3).setCellValue("stContactNumber");
                    row.createCell(4).setCellValue("stClass");

            int dataRowIndex = 1;

                for(Student st:students)
                {
                    // insert data into row
                    HSSFRow dataRow = sheet.createRow(dataRowIndex);
                            dataRow.createCell(0).setCellValue(st.getStId());
                            dataRow.createCell(1).setCellValue(st.getStName());
                            dataRow.createCell(2).setCellValue(st.getStMail());
                            dataRow.createCell(3).setCellValue(st.getStContactNumber());
                            dataRow.createCell(4).setCellValue(st.getStClass());
                            dataRowIndex++;
                }

                ServletOutputStream outputStream = response.getOutputStream();
                workbook.write(outputStream);
                workbook.close();
                outputStream.close();
        }


}
