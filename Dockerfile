# Docker file - instructions(bluePrint) to create the docker image,
FROM openjdk:21
RUN mkdir /usr/app/
COPY target/My_APP.jar /usr/app
WORKDIR /usr/app/
ENTRYPOINT [ "java","-jar","My_APP.jar" ]
